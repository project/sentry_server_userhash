<?php

/**
 * @file
 * Page callbacks and other review page related functions for the user hash plugin.
 */

/**
 * Page callback for the status overview page.
 */
function sentry_server_userhash_overview($node) {
  $result = db_query('SELECT uid, hash, temporary_hash FROM {sentry_userhash_clients} WHERE nid = %d', $node->nid);

  $is_error = FALSE;
  $rows = array();
  while ($user = db_fetch_object($result)) {
    $row = array();

    if ($user->hash != $user->temporary_hash) {
      $message = t('Error');
      $icon = 'misc/watchdog-error.png';
      $text = t('User changed!');
      $is_error = TRUE;
    }
    else {
      $message = t('Ok');
      $icon = 'misc/watchdog-ok.png';
      $text = t('User not changed.');
    }
    $row = array(
      basename($user->uid),
      theme('image', $icon, $message, $message) .' '. $text,
    );
    $rows[] = $row;
  }
  if (!count($rows)) {
    $rows[][] = array('data' => t('No hashes have been recorded yet.'), 'colspan' => 2);
  }

  $header = array(t('User ID.'), t('Status'));
  if ($is_error) {
    $output = drupal_get_form('sentry_server_userhash_approval_form', $node);
  }
  $output .= theme('table', $header, $rows);
  return $output;
}

function sentry_server_userhash_approval_form($form_state, $node) {
  $form = array();
  $form['nid'] = array(
    '#type' => 'value',
    '#value' => $node->nid,
  );
  $form['submit'] = array(
    '#value' => t("It's ok"),
    '#type' => 'submit',
  );
  return $form;
}

function sentry_server_userhash_approval_form_submit($form, &$form_state) {
  db_query('UPDATE {sentry_userhash_clients} SET hash = temporary_hash WHERE nid = %d', $form_state['values']['nid']);
}
